<img src="workshop/images/SHIFTA_LOGO_581x189_01.png" width="200px"/>

# Introducción

**¡Hola! ¡Bienvenid@ a nuestro Handbook!** La idea es hacer de este repo, un mapa y guía (breve) a través de la cual abrimos SHIFTA a quienes deseen conocernos más. 
 
Este manual es producto vivo del co-desarrollo de la Tribu; una construcción colaborativa, iterativa y en beta constante. En SHIFTA somos lo que hacemos, lo que decimos y lo que compartimos con nuestro equipo. 
 
Si esta primera aproximación, comienza a despertar tu pasión y te interesa conocernos más, te invitamos a conversar a través de cualquiera de los espacios que abrimos a la comunidad y que vas a conocer en la sección ¨Redes¨. 


