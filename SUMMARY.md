<!-- Rules of SUMMARY.md are here: https://docs.gitbook.com/integrations/github/content-configuration#summary -->
<!-- All headings MUST be THREE hashmarks (###) -->
<!-- Indented bullets (4 spaces) will make the first line be a section -->

### Handbook

* [Introducción](README.md)

### Why SHIFTA?

* [¿Quiénes somos?](workshop/why-shifta/README.md)


### Descripción de Roles :
* [Ingeniería](workshop/roles/README.md)
* [Tech Lead](workshop/descripcion-tl/README.md)
* [Software Engineer Senior](workshop/descripcion-sesr/README.md)
* [Software Engineer Semi-Senior](workshop/descripcion-sessr/README.md)
* [Software Engineer Junior](workshop/descripcion-sej/README.md)


### Redes

* [Contactanos](workshop/redes/README.md)


