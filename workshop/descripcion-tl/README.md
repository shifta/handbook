<img src="https://media-exp1.licdn.com/dms/image/C4D16AQHD8PEx4WucJw/profile-displaybackgroundimage-shrink_200_800/0?e=1598486400&v=beta&t=XouNVOwgw5vgdpuHUfIQ7HanMOIjrG0TPfMU5fDhx_M" width="100%"/>


## Líder Técnico

Tendrás la responsabilidad sobre el diseño y la implementación de soluciones y arquitecturas modernas de nube e híbridas, de alto desempeño y resiliencia. Llevarás el proceso de discovery y delivery desde el comienzo hasta la entrega, asegurando el performance y calidad de las implementaciones y del trabajo del equipo. Para ello, orientarás a clientes, capacitarás a tu equipo y asegurarás la calidad del proceso de desarrollo. 

Tus diseños, soluciones y propuestas requerirán usar tecnologías y técnicas de punta, y los discutirás y defenderás con otros shifters. Probarás y conocerás nuevas y emocionantes tecnologías desarrollando POCs y Spikes que te permitan entender la factibilidad de las mismas. 

Para asegurar la calidad en el delivery, diseñarás, implementarás e iterarás las mejores prácticas de CI/CD, obteniendo métricas del rendimiento de las entregas.

Tendrás el desafío y el honor de liderar, nutrir y entrenar equipos de desarrollo multidisciplinarios de alto desempeño desde un punto de vista técnico. Sé el mentor que ayude a mejorar técnica y personalmente a otros shiters, desbloqueando su verdadero potencial. 


### Principales responsabilidades y Desafíos

* Ayude al equipo a proporcionar estimaciones para el trabajo futuro.
* Defina las tecnologías y la arquitectura para usar desde nuestro panorama tecnológico, adaptándose a los cambios a través de la creación del producto, asegurando desde el lado técnico que los objetivos del producto se alcanzarán dentro del tiempo y el presupuesto.
* Defina las responsabilidades técnicas de cada desarrollador dentro de un proyecto, proporcionando comentarios oportunos al equipo, al PM y a la oficina técnica.
* Revise y ayude a su equipo a revisar el código del otro, buscando la mejor calidad y un enfoque pragmático.
* Identifique posibles riesgos técnicos, problemas y comuníquelos a tiempo y los lleve con liderazgo. Trabaje con el equipo y la Oficina Técnica para mitigarlos
 
### Experiencia

* Conocimiento profundo de al menos una oferta en la nube, como Azure, AWS o GCP.
* Experiencia en diseño y arquitectura de sistemas distribuidos.
* Experiencia liderando un equipo técnico multidisciplinario.
* Grandes habilidades y prácticas de ingeniería junto con sólidos fundamentos de CS.
* Experiencia en diseño y desarrollo de API y servicios RESTFul.
* Conocimiento profundo y experiencia con al menos un lenguaje / tecnología, por ejemplo, Go, node.js, .NET Core, etc.
* Familiarizado con los SDK en la nube basados en CLI y las prácticas de ingeniería como Infraestructura como código, entre otros (IaC, IaaS, PaaS)
* Experiencia con Docker y Kubernetes y herramientas y tecnologías relacionadas.
* Experiencia con bases de datos SQL y NoSQL.
* Experiencia en metodología ágil (Scrum, XP, Modern Agile).
* Habilidades de automatización e instrumentación.
* +5 años de experiencia de desarrollo.
* +2 años de experiencia líder.
* Inglés.
* Experiencia con Docker y Kubernetes y tecnologías de herramientas relacionadas.
*Experiencia trabajando con clientes de diferentes tamaños, desde grandes empresas hasta pequeños startups.

### Competencias:

* Fuertes habilidades analíticas y de resolución de problemas.
* Jugador de equipo con sólidas habilidades de comunicación y presentación.
* Capacidad para comunicar de manera efectiva soluciones técnicas a personas no técnicas.
* Capacidad para predecir y comunicar oportunamente la desviación del presupuesto u horario.
* Creativo e innovador.
* Capacidad para recibir y ofrecer comentarios de forma regular, ansioso por crecer y desarrollarse dentro y fuera de la organización.

 

