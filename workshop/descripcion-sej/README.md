<img src="https://media-exp1.licdn.com/dms/image/C4D16AQHD8PEx4WucJw/profile-displaybackgroundimage-shrink_200_800/0?e=1598486400&v=beta&t=XouNVOwgw5vgdpuHUfIQ7HanMOIjrG0TPfMU5fDhx_M" width="100%"/>


## Junior Software Engineer

Como Junior Software Engineer tu responsabilidad principal, será apoyar en el desarrollo, implementación y administración de soluciones empresariales de software, mientras ayudas a mejorar el proceso de delivery. Tendrás el desafío y honor de desarrollar código de calidad, testearás e implementarás software y cambios de configuración, mientras apoyas a que el diseño de las aplicaciones cumplan los requerimientos funcionales y técnicos, dentro de un equipo multidisciplinario.

Tu colaboración, no sólo aportará valor al ciclo de delivery, sino también en el ciclo de discovery de las soluciones de software en las que participes, mientras alimentas tu curiosidad y ganas de aprender, escribiendo código de alta calidad, debugueando y resolviendo problemas dentro del mismo.

Serás protagonista a través del co-diseño de tu plan de crecimiento y aprendizaje, discutiendo y negociando con el equipo interno y basado en tus intereses personales y organizacionales.

Investigarás y compatirás información concerniente a las tecnologías que tus proyectos utilizan, y usarás tu creatividad para incorporar estos conocimientos en tus desarrollos de manera que cumplan y se adapten a la expectativas de tu equipo interno y del cliente. Trabajarás y te acompañaremos para que puedas integrarte a un entorno altamente colaborativo y amigable, al mismo tiempo que aprendes a adaptarte a la cultura y ambiente de la organización.

### Principales responsabilidades y Desafíos

* Desarrollarás componentes que ayuden al equipo a alcanzar su máximo desempeño.
* Tú código será revisado, al mismo tiempo que ayudarás al equipo a revisar el código de cada uno.
* Asegurar que la solución final del software cumpla con los requerimientos y expectativas de tu equipo y del cliente.
* Recibir y otorgar feedback de manera regular.

### Experiencia

* Conocimientos básicos de prácticas de ingeniería de software y/o fundamentos de ciencias de la computación.
* Familiarizado con alguno de los siguientes lenguajes/frameworks: Go, ES6, Typescript, dotnet core, Python, Ruby, Java, Node.js.
* Conocimiento básico de bases de datos y SQL.
* Inglés de lectura técnica.


