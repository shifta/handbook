# Ingeniería

## Be a Shifter

_¨En SHIFTA crecemos en base a las capacidades del talento, dejando de lado cualquier tipo de sesgo. Nuestra capacidad de **#Innovación** está fuertemente relacionada con la **#Diversidad** que cultivamos.
Todos los SHIFTERS son líderes de su carrera desde el primer día en la tribu y cuentan con el apoyo de su mentor. Impulsandote siempre a tomar el camino de la **#FormaciónAcadémica**, el **#FeedForward**, la **#AutoGestión** y el **#CoDesarrollo**¨._


