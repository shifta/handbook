<img src="https://media-exp1.licdn.com/dms/image/C4D16AQHD8PEx4WucJw/profile-displaybackgroundimage-shrink_200_800/0?e=1598486400&v=beta&t=XouNVOwgw5vgdpuHUfIQ7HanMOIjrG0TPfMU5fDhx_M" width="100%"/>


## Senior Software Engineer

Tendrás la responsabilidad sobre la implementación de productos de software modernos, innovadores y de calidad. Llevarás el proceso de discovery y delivery desde el comienzo  hasta la entrega, asegurando la performance y calidad de tus soluciones. Junto con tu equipo, ayudarás a implementar, enforzar y llevar prácticas de Delivery continuo y a guiar a miembros menos experimentados del equipo a brillar.

Tendrás el desafío y honor de desarrollar código de calidad, implementarás y llevarás las mejores prácticas de software, mientras apoyas a que el diseño de las aplicaciones cumplan los requerimientos funcionales y técnicos, dentro de un equipo multidisciplinario. Esperamos construyas, discutas, defiendas y retroalimentes tus propuestas de diseño y arquitectura con tus colaboradores.

Investigarás y compatirás con tus colaboradores información concerniente a las tecnologías que tus proyectos utilizan, y usarás tu creatividad para incorporarlos de manera efectiva en el desarrollo de tu equipo. Trabajarás y fomentarás un entorno altamente colaborativo y amigable, trabajando codo a codo con otros SHIFTERS.

### Principales responsabilidades y Desafíos

* Guiar y coachear a colaboradores menos experimentados a través de feed-forward, pair programing o código de muestra.
* Analiza y comprende bases de código, mientras reconoces oportunidades de mejora.
* Sé punta de lanza en el diseño de soluciones, en la resolución de issues y debuggeo a través de tu expertise.
* Desarrollar componentes core y estructurales para permitir al equipo alcanzar su desempeño y calidad óptimos.
* Implementa prácticas de CI/CD.
* Revisar y asegurar la calidad del código y prácticas dentro del código de tus soluciones.

### Experiencia

* Experiencia con: Go, ES6, Typescript, dotnet core, Python, Ruby, Java, Node.js.
* Gran conocimiento de bases de datos relacionales, no relacionales; además de SQL.
* En marcos de trabajo ágiles.
* Gran experiencia en el desarrollo de servicios y APIs Web.
* Al menos 3 años de experiencia




